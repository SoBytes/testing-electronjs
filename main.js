const electron = require('electron')
// Module to control application life.
const app = electron.app
// Module to create native browser window.
const BrowserWindow = electron.BrowserWindow

var basepath = app.remote;

const path = require('path')
const fs = require('fs')
const url = require('url')
const rootPath = require('electron-root-path').rootPath

const platform = require('os')

const shell = require('shelljs')
shell.config.execPath = shell.which('node');

var ffmpeg = require('ffmpeg-static-electron');
console.log(ffmpeg.path);

const ffmpegPath = ffmpeg.path; //rootPath + '/resources/mac/bin/ffmpeg';
console.log('ffmpegPath',ffmpegPath);

const wav2jsonPath = rootPath + '/resources/mac/bin/wav2json';
console.log('wav2jsonPath',wav2jsonPath);

const logoPath = rootPath + '/resources/mac/bin/logo.png';
console.log('logoPath',logoPath);

console.log(app.getPath('temp'));

/*
const ffmpegPath = rootPath + '/Contents/Resources/bin/ffmpeg';
console.log('ffmpegPath',ffmpegPath);

const wav2jsonPath = rootPath + '/Contents/Resources/bin/wav2json';
console.log('wav2jsonPath',wav2jsonPath);

const logoPath = rootPath + '/Contents/Resources/bin/logo.png';
console.log('logoPath',logoPath);
*/

const {
    ipcMain
} = require('electron');

const {
    event_keys
} = require('./constants')


// Keep a global reference of the window object, if you don't, the window will
// be closed automatically when the JavaScript object is garbage collected.
let mainWindow

function createWindow() {
    // Create the browser window.
    mainWindow = new BrowserWindow({
        width: 400,
        height: 380,
        frame: false,
        transparent: true,
        resizable: false,
        webPreferences: {
            webSecurity: false
        }
    })

    // and load the index.html of the app.
    mainWindow.loadURL(url.format({
        pathname: path.join(__dirname, 'index.html'),
        protocol: 'file:',
        slashes: true
    }))

    // Load the logo
    mainWindow.webContents.on('did-finish-load', function() {
        mainWindow.webContents.send('set-image', logoPath);
    });

    // Open the DevTools.
    //mainWindow.webContents.openDevTools()

    // Emitted when the window is closed.
    mainWindow.on('closed', function() {
        // Dereference the window object, usually you would store windows
        // in an array if your app supports multi windows, this is the time
        // when you should delete the corresponding element.
        mainWindow = null
    })

}

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.on('ready', createWindow)

// Quit when all windows are closed.
app.on('window-all-closed', function() {
    // On OS X it is common for applications and their menu bar
    // to stay active until the user quits explicitly with Cmd + Q
    if (process.platform !== 'darwin') {
        app.quit()
    }
})

app.on('activate', function() {
    // On OS X it's common to re-create a window in the app when the
    // dock icon is clicked and there are no other windows open.
    if (mainWindow === null) {
        createWindow()
    }
})

ipcMain.on('ondragstart', (event, filePath) => {

    mainWindow.webContents.send('drag-data', 'Processing...');

    const {
        ext,
        name,
        dir
    } = path.parse(filePath)

    let fps     = 30;
    let bifSecs = (fps * 1000);

    try {

        // Create frames
        let ffm = shell.exec('mkdir ' + app.getPath('temp') + '/process | ' + ffmpegPath + ' -i ' + filePath + ' ' + app.getPath('temp') + 'process/' + name + '.wav', {
            async: true
        });
        //-qscale:v 31
        ffm.stdout.on('data', (data) => {
            console.log('done');
        });
        ffm.stdout.on('finish', () => {

            // Create bif
            let bif = shell.exec('' +  wav2jsonPath + ' -d ' + app.getPath('temp') + 'process/' + name + '.wav', {
                async: true
            });
            bif.stdout.on('data', (data) => {
                console.log('done',data);
            });
            bif.stdout.on('finish', (data) => {

                // Move bif
                let mvBif = shell.exec('mv ' + app.getPath('temp') + 'process/' + name + '.wav.json ' + dir + ' ', {
                    async: true
                });
                mvBif.stdout.on('data', (data) => {
                    console.log('done',data);
                });
                mvBif.stdout.on('finish', (data) => {

                    // Run clean up
                    const directory = app.getPath('temp') + 'process';

                    fs.readdir(directory, (err, files) => {
                        if (err) throw err;

                        for (const file of files) {
                            fs.unlink(path.join(directory, file), err => {
                                if (err) throw err;
                            });
                        }
                        
                    });
                    
                    mainWindow.webContents.send('drag-data', 'Finished');

                });

            });

        });

    } catch (error) {

        console.log(error)

    }

})