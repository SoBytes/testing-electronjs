import sys
import os
import struct
import array
from optparse import OptionParser

parser = OptionParser()
parser.add_option(  "-d", "--directory", dest="directory", type='string', default=0,
                    help="Enter the directory your images are stored in")
parser.add_option(  "-i", "--interval", dest="interval", type='int', default=1,
                    help="Interval between images in seconds (default is 10)")
(options, args) = parser.parse_args()

filename = "%s.bif" % (options.directory)

magic = [0x89,0x42,0x49,0x46,0x0d,0x0a,0x1a,0x0a]
version = 0

files = os.listdir("%s" % (options.directory))
images = []
for image in files:
    if image[-4:] == '.jpg':
        images.append(image)
images.sort()
images = images[1:]

f = open(filename, "wb")
array.array('B', magic).tofile(f)
f.write(struct.pack("<I1", version))
f.write(struct.pack("<I1", len(images)))
f.write(struct.pack("<I1", 1000 * options.interval))
array.array('B', [0x00 for x in xrange(20,64)]).tofile(f)

bifTableSize = 8 + (8 * len(images))
imageIndex = 64 + bifTableSize
timestamp = 0

# Get the length of each image
for image in images:
    statinfo = os.stat("%s/%s" % (options.directory, image))
    f.write(struct.pack("<I1", timestamp))
    f.write(struct.pack("<I1", imageIndex))

    timestamp += 1
    imageIndex += statinfo.st_size

f.write(struct.pack("<I1", 0xffffffff))
f.write(struct.pack("<I1", imageIndex))

# Now copy the images
for image in images:
    data = open("%s/%s" % (options.directory, image), "rb").read()
    f.write(data)

f.close()